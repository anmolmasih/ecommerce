from django.shortcuts import render
from django.http import JsonResponse
import json
import datetime
from .models import * 
from .utils import cookieCart, cartData, guestOrder


from django.http import JsonResponse
from .models import Product

from rest_framework.decorators import api_view
from rest_framework.response import Response
from store.serializers import ProductSerializer

from django.contrib.auth.decorators import login_required
from accounts.decorators import unauthenticated_user, allowed_users, admin_only


@login_required(login_url='login')
def store(request):
	data = cartData(request)

	cartItems = data['cartItems']
	order = data['order']
	items = data['items']

	products = Product.objects.all()
	print("print product Here")
	prd = Product.objects.filter(address="karnataka")
	print(prd)
	print("Printing lat long of center")
	center_cord=get_ip()
	print(center_cord)
	print("Print only Addresses Here")
	prod= Product.objects.values_list('address')
	print(prod[0])


	context = {'products':products, 'cartItems':cartItems}
	return render(request, 'store/store.html', context)


@login_required(login_url='loginPage')
def cart(request):
	data = cartData(request)

	cartItems = data['cartItems']
	order = data['order']
	items = data['items']

	context = {'items':items, 'order':order, 'cartItems':cartItems}
	return render(request, 'store/cart.html', context)

@login_required(login_url='loginPage')
def checkout(request):
	data = cartData(request)
	
	cartItems = data['cartItems']
	order = data['order']
	items = data['items']

	context = {'items':items, 'order':order, 'cartItems':cartItems}
	return render(request, 'store/checkout.html', context)

@login_required(login_url='loginPage')
def updateItem(request):
	data = json.loads(request.body)
	productId = data['productId']
	action = data['action']
	print('Action:', action)
	print('Product:', productId)

	customer = request.user.customer
	product = Product.objects.get(id=productId)
	order, created = Order.objects.get_or_create(customer=customer, complete=False)

	orderItem, created = OrderItem.objects.get_or_create(order=order, product=product)

	if action == 'add':
		orderItem.quantity = (orderItem.quantity + 1)
	elif action == 'remove':
		orderItem.quantity = (orderItem.quantity - 1)

	orderItem.save()

	if orderItem.quantity <= 0:
		orderItem.delete()

	return JsonResponse('Item was added', safe=False)

@login_required(login_url='loginPage')
def processOrder(request):
	transaction_id = datetime.datetime.now().timestamp()
	data = json.loads(request.body)

	if request.user.is_authenticated:
		customer = request.user.customer
		order, created = Order.objects.get_or_create(customer=customer, complete=False)
	else:
		customer, order = guestOrder(request, data)

	total = float(data['form']['total'])
	order.transaction_id = transaction_id

	if total == order.get_cart_total:
		order.complete = True
	order.save()

	if order.shipping == True:
		ShippingAddress.objects.create(
		customer=customer,
		order=order,
		address=data['shipping']['address'],
		city=data['shipping']['city'],
		state=data['shipping']['state'],
		zipcode=data['shipping']['zipcode'],
		)

	return JsonResponse('Payment submitted..', safe=False)

def lat_log(adds):
    import requests
    import geopy
    import geocoder
    from geopy.geocoders import Nominatim
    geocode_lo = adds
    g = geocoder.arcgis(geocode_lo)
    #print(g.geojson)
    g.geojson
    lo_lat = str(g.geojson['features'][0]['properties']['lat'])
    lo_lng = str(g.geojson['features'][0]['properties']['lng'])
    print("lat:"+lo_lat)
    print("lat:"+lo_lng)
    #reverse geocode
    geolocator = Nominatim(user_agent="xyz")
    location = geolocator.reverse(""+lo_lat+","+lo_lng+"")
    return lo_lat,lo_lng
# function for getting IP Address
from requests import get

from ip2geotools.databases.noncommercial import DbIpCity
def get_ip():
    ip=get("https://api.ipify.org").text
    response= DbIpCity.get(ip,api_key="free")
    latitude=response.latitude
    longitude=response.longitude
    return latitude,longitude

# center point mean your live location
from geopy import distance
from geopy.distance import geodesic
#For API
@api_view(['GET'])
def location_api(request):
	#products= Product.objects.all()
	#products= Product.objects.only('address')
	prod= Product.objects.values_list('address')
	center_cord=get_ip()
	radius = 5 # in kilometer
	for i in prod:
		test_loc=lat_log(i)
		dis=geodesic(center_cord,test_loc).kilometers
		if dis <= radius:
				print("{} point is inside the {} km radius from {} coordinate".format(test_loc, radius, center_cord))
				print(Product.objects.filter(address__icontains=i[0]))
				
		else:
    			print("{} point is outside the {} km radius from {} coordinate".format(test_loc, radius, center_cord))
	#products= Product.objects.filter(address=i[0])
	#products= Product.objects.filter(address__icontains__in=str(i[0]).split(','))
	products=Product.objects.filter(address__icontains=i[0])
	serializer=ProductSerializer(products,many=True)
	return Response(serializer.data)
