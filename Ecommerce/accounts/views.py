from django.shortcuts import render,redirect,HttpResponse

from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from django.contrib import messages

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group

from .decorators import unauthenticated_user, allowed_users, admin_only

# Create your views here.

@unauthenticated_user

def registerPage(request):
    #form = UserCreationForm()
    form = CreateUserForm()
    if request.method == 'POST':
        #form = UserCreationForm(request.POST)
        form = CreateUserForm(request.POST)
        
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name='customer')
            user.groups.add(group)
            messages.success(request, 'Account was created for ' + username)

            return redirect('loginPage')

    context = {'form':form}
    return render(request,'accounts/register.html',context)


@unauthenticated_user
def loginPage(request):
    
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)

        username=request.POST.get('username')
        password=request.POST.get('password')

        user = authenticate(request,username=username,password=password)

        if user is not None:
            login(request, user)
            return redirect('user-page')
    context ={}
    return render(request,'accounts/login.html',context)

def logoutUser(request):
    logout(request)
    return redirect('loginPage')

@login_required(login_url='login')
@admin_only
def userPage(request):
    context={}
    return render(request,'accounts/user.html',context )