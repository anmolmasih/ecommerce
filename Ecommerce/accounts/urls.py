from django.urls import path

from . import views

urlpatterns = [
	#Leave as empty string for base url
	path('register/', views.registerPage, name="registerPage"),
	path('', views.loginPage,name="loginPage"),
	path('logout/', views.logoutUser,name="logout"),
	path('user/', views.userPage, name="user-page"),

]